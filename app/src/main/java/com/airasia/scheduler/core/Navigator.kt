package com.airasia.scheduler.core

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.airasia.scheduler.R
import com.airasia.scheduler.data.model.Engineers
import com.airasia.scheduler.ui.engineers.EngineersFragment
import com.airasia.scheduler.ui.schedule.ScheduleFragment
import javax.inject.Inject

class Navigator @Inject constructor(private val activity: AppCompatActivity) {
    private val containerId: Int = R.id.container
    private val fragmentManager: FragmentManager = activity.supportFragmentManager

    fun navigateEngineers() {
        replaceFragment(EngineersFragment())
    }

    fun navigateSchedule(value: ArrayList<Engineers>) {
        replaceFragment(ScheduleFragment.newInstance(value))
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = fragmentManager
            .beginTransaction()
            .replace(containerId, fragment, null)

        if (fragmentManager.isStateSaved) {
            transaction.commitAllowingStateLoss()
        } else {
            transaction.commit()
        }
    }
}