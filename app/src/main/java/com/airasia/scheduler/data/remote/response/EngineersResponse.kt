package com.airasia.scheduler.data.remote.response

import com.squareup.moshi.Json

data class EngineersResponse(
    @Json(name = "engineers") val engineers: List<EngineersSchema>
)