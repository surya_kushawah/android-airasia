package com.airasia.scheduler.data.remote.mapper

import com.airasia.scheduler.data.model.Engineers
import com.airasia.scheduler.data.remote.response.EngineersSchema

fun List<EngineersSchema>.toEngineersEntities(): List<Engineers> =
    map {
        it.toEngineersEntity()
    }

fun EngineersSchema.toEngineersEntity(): Engineers {
    return Engineers(
        id, name
    )
}