package com.airasia.scheduler.data.repository

import com.airasia.scheduler.data.model.Engineers
import io.reactivex.Single

interface EngineersRepository {
    fun loadEngineers(): Single<List<Engineers>>
}