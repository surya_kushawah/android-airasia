package com.airasia.scheduler.data.remote

import com.airasia.scheduler.data.remote.response.EngineersResponse
import io.reactivex.Single
import retrofit2.http.GET

interface EngineersApi {

    @GET("5dd561ad3300008f87f380df")
    fun loadEngineers(): Single<EngineersResponse>
}