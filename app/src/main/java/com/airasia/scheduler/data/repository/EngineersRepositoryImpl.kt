package com.airasia.scheduler.data.repository

import com.airasia.scheduler.data.model.Engineers
import com.airasia.scheduler.data.remote.EngineersApi
import com.airasia.scheduler.data.remote.mapper.toEngineersEntities
import com.airasia.scheduler.util.rx.SchedulerProvider
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class EngineersRepositoryImpl @Inject constructor(
    private val api: EngineersApi,
    private val schedulerProvider: SchedulerProvider
) : EngineersRepository {

    override fun loadEngineers(): Single<List<Engineers>> = api.loadEngineers()
        .map {
            return@map it.engineers.toEngineersEntities()
        }
        .onErrorReturn { error ->
            Timber.e(error.toString())

            return@onErrorReturn emptyList<Engineers>()
        }
        .subscribeOn(schedulerProvider.io())

}