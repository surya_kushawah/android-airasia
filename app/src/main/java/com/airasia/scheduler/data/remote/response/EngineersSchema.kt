package com.airasia.scheduler.data.remote.response

import com.squareup.moshi.Json

data class EngineersSchema(
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String
)