package com.airasia.scheduler.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Engineers(
    val id: Int,
    val name: String
) : Parcelable