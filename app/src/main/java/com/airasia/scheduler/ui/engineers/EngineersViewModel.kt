package com.airasia.scheduler.ui.engineers

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.airasia.scheduler.core.Result
import com.airasia.scheduler.core.toResult
import com.airasia.scheduler.data.model.Engineers
import com.airasia.scheduler.data.repository.EngineersRepository
import com.airasia.scheduler.util.ext.map
import com.airasia.scheduler.util.ext.toLiveData
import com.airasia.scheduler.util.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class EngineersViewModel @Inject constructor(
    private val repository: EngineersRepository,
    private val schedulerProvider: SchedulerProvider
) : ViewModel(), LifecycleObserver {

    private val compositeDisposable = CompositeDisposable()

    private val _engineers: LiveData<Result<List<Engineers>>> =
        repository.loadEngineers()
            .toResult(schedulerProvider)
            .toLiveData()

    val engineers: LiveData<Result<List<Engineers>>> = _engineers


    private val _isLoading: LiveData<Boolean> by lazy {
        _engineers.map {
            it.inProgress
        }
    }
    val isLoading: LiveData<Boolean> = _isLoading

    fun onRefresh() {
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
