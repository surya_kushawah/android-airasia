package com.airasia.scheduler.ui.schedule.model

data class ScheduleDetails(
    val day: String,
    val dayShift: String,
    val nightShift: String
)