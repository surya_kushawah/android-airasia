package com.airasia.scheduler.ui.main.builder

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.airasia.scheduler.di.ViewModelKey
import com.airasia.scheduler.ui.engineers.EngineersFragment
import com.airasia.scheduler.ui.engineers.EngineersViewModel
import com.airasia.scheduler.ui.main.MainActivity
import com.airasia.scheduler.ui.schedule.ScheduleFragment
import com.airasia.scheduler.ui.schedule.ScheduleViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import com.airasia.scheduler.ui.main.MainViewModel as MainViewModel1

@Module
interface MainActivityModule {
    @Binds
    fun providesAppCompatActivity(mainActivity: MainActivity): AppCompatActivity

    @ContributesAndroidInjector
    fun contributeEngineersFragment(): EngineersFragment

    @ContributesAndroidInjector
    fun contributeScheduleFragment(): ScheduleFragment

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel1::class)
    fun bindMainViewModel(
        mainViewModel: MainViewModel1
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EngineersViewModel::class)
    fun bindEngineersViewModel(
        engineersViewModel: EngineersViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScheduleViewModel::class)
    fun bindScheduleViewModel(
        scheduleViewModel: ScheduleViewModel
    ): ViewModel

}