package com.airasia.scheduler.ui.engineers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.airasia.scheduler.R
import com.airasia.scheduler.core.Navigator
import com.airasia.scheduler.core.Result
import com.airasia.scheduler.core.ViewModelFactory
import com.airasia.scheduler.util.autoCleared
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.engineers_fragment.*
import timber.log.Timber
import javax.inject.Inject

class EngineersFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var navigator: Navigator

    private var adapter by autoCleared<EngineersAdapter>()


    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(EngineersViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.engineers_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        viewModel.engineers.observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Result.Success -> {
                    adapter.submitList(result.data)
                }
                is Result.Failure -> {
                    Timber.wtf(result.errorMessage)
                }
            }
        })
        viewModel.isLoading.observe(viewLifecycleOwner, Observer { isLoading ->
            isLoading.let {
                refreshLayout.isRefreshing = isLoading
                refreshLayout.isEnabled = false;
            }
        })

        buttonSchedule.setOnClickListener {
            if (adapter.engineers.isNotEmpty())
                navigator.navigateSchedule(ArrayList(adapter.engineers))
        }
    }

    private fun initRecyclerView() {
        adapter = EngineersAdapter()
        val layoutManager = LinearLayoutManager(requireContext())
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter


    }

}
