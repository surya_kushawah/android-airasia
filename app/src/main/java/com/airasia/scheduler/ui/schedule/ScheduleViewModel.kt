package com.airasia.scheduler.ui.schedule

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.airasia.scheduler.data.model.Engineers
import com.airasia.scheduler.ui.schedule.factory.ScheduleFactory
import com.airasia.scheduler.ui.schedule.model.ScheduleDetails
import com.airasia.scheduler.util.Day
import com.airasia.scheduler.util.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ScheduleViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider
) : ViewModel(), LifecycleObserver {
    private val compositeDisposable = CompositeDisposable()
    val WEEK_DAYS = 7

    val secludeLiveData = MutableLiveData<List<ScheduleDetails>>()


    fun getWeeklyShiftSchedule(
        engineers: List<Engineers>,
        scheduleFactory: ScheduleFactory
    ) {
        var weeklySchedule: MutableList<ScheduleDetails> = mutableListOf();
        val scheduleMatrix = scheduleFactory.generateSchedule()

        for (i in scheduleMatrix.indices) {
            val scheduleDetails = ScheduleDetails(
                Day.values()[i % WEEK_DAYS].name,
                engineers[scheduleMatrix[i][0]!!].name,
                engineers[scheduleMatrix[i][1]!!].name
            )
            weeklySchedule.add(scheduleDetails)
        }
        secludeLiveData.value = weeklySchedule
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
