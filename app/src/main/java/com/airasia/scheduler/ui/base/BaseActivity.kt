package com.airasia.scheduler.ui.base

import android.annotation.SuppressLint
import dagger.android.support.DaggerAppCompatActivity

@SuppressLint("Registered")
open class BaseActivity : DaggerAppCompatActivity()