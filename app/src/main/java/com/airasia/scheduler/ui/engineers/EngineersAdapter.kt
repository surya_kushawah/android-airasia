package com.airasia.scheduler.ui.engineers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.airasia.scheduler.R
import com.airasia.scheduler.data.model.Engineers
import kotlinx.android.synthetic.main.item_engineers.view.*

class EngineersAdapter :
    RecyclerView.Adapter<EngineersAdapter.ViewHolder>() {
    var engineers = emptyList<Engineers>()
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_engineers, parent, false)
    )

    fun submitList(engineerList: List<Engineers>) {
        engineers = engineerList
        notifyDataSetChanged()
    }

    override fun getItemCount() = engineers.size
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(engineers[position])
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name = view.textViewName
        fun bind(engineers: Engineers) {
            name.text = engineers.name

        }
    }

}