package com.airasia.scheduler.ui.schedule

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.airasia.scheduler.R
import com.airasia.scheduler.ui.schedule.model.ScheduleDetails
import kotlinx.android.synthetic.main.item_schedule_details.view.*

class ScheduleAdapter :
    RecyclerView.Adapter<ScheduleAdapter.ViewHolder>() {
    private var scheduleList = emptyList<ScheduleDetails>()
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_schedule_details, parent, false)
    )

    fun submitList(scheduleDetails: List<ScheduleDetails>) {
        scheduleList = scheduleDetails
        notifyDataSetChanged()
    }

    override fun getItemCount() = scheduleList.size
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(scheduleList[position])
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val day = view.textViewDay
        private val dayShift = view.textViewDayShift
        private val nightShift = view.textViewNightShift
        fun bind(scheduleDetail: ScheduleDetails) {
            day.text = scheduleDetail.day
            dayShift.text = scheduleDetail.dayShift
            nightShift.text = scheduleDetail.nightShift

        }
    }

}