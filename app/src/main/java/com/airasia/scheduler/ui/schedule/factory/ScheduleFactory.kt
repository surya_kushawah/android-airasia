package com.airasia.scheduler.ui.schedule.factory

import com.airasia.scheduler.data.model.Engineers
import kotlin.math.ceil

class ScheduleFactory(
    private val engineersList: List<Engineers>,
    private val numberOfDay: Int,
    var numberOfShift: Int
) {
    companion object {
        const val HIGH_PRIORITY = 0.1
        const val ZERO_PRIORITY = 0.0
        const val LOW_PRIORITY = 0.01
        const val NORMAL_PRIORITY = 0.02

    }

    var numberGenerator = RandomNumberGenerator()

    // keep frequency of user based of id
    var frequencyMap: HashMap<Int, Int> = HashMap()

    private var count = 0

    private val maxFrequencyCount = numberOfDay * numberOfShift % engineersList.size
    private val higherFrequency =
        ceil(((numberOfDay * numberOfShift).toDouble()) / engineersList.size).toInt()
    private val lowerFrequency = higherFrequency - 1

    init {
        buildEngineersList(engineersList.size - 1)
    }

    /**
     * Build RandomNumberGenerator entries with same priority
     */
    private fun buildEngineersList(size: Int) {
        for (i in 0..size) {
            numberGenerator.addNumber(i, HIGH_PRIORITY)
        }

    }

    /**
     * Generate complete schedule
     * @return 14*2 matrix
     */
    fun generateSchedule(): Array<Array<Int?>> {
        val matrix =
            Array(numberOfDay) { arrayOfNulls<Int>(numberOfShift) }
        // row and column starting from  0 than size should be -1
        val matrixRow = numberOfDay - 1
        val matrixCol = numberOfShift - 1
        for (i in 0..matrixRow) {
            for (j in 0..matrixCol) {
                val consecutiveEngineers: MutableList<Int> = ArrayList()
                if (i == 0 && j == 0) {

                } else if (i == 0 && j == 1) {
                    consecutiveEngineers.add(matrix[i][j - 1]!!)
                } else if (j == 0) {
                    consecutiveEngineers.add(matrix[i - 1][j]!!)
                    consecutiveEngineers.add(matrix[i - 1][j + 1]!!)
                } else {
                    consecutiveEngineers.add(matrix[i - 1][j - 1]!!)
                    consecutiveEngineers.add(matrix[i - 1][j]!!)
                    consecutiveEngineers.add(matrix[i][j - 1]!!)
                }
                val index: Int =
                    generateRandomNumberWithConstraint(
                        numberGenerator,
                        consecutiveEngineers
                    )

                enterMatrixData(engineersList[index].id, matrix, i, j)
            }
        }
        return matrix
    }

    /**
     * Enter the value in Matrix
     *
     * @param id
     * @param matrix
     * @param matrix Row
     * @param matrix Col
     */
    private fun enterMatrixData(
        id: Int,
        matrix: Array<Array<Int?>>,
        matrixRow: Int,
        matrixCol: Int
    ) {
        matrix[matrixRow][matrixCol] = id
    }

    /**
     * Generate number with Constraint matrix 14*2 so constraint is number is not last 3 position
     *
     * @param numberGenerator - RandomNumberGenerator that generate number based on priority
     * @param consecutiveEngineers - Hold of last 3 position of Matrix
     * @return return index of employee
     */
    private fun generateRandomNumberWithConstraint(
        numberGenerator: RandomNumberGenerator,
        consecutiveEngineers: List<Int>
    ): Int {
        val index = numberGenerator.getRandomNumber()

        // get engineer Id based on index of
        val engineerId = engineersList[index].id
        /**
         * if generated number is consecutiveEngineers list in this matrix  generate another number
         */
        if (consecutiveEngineers.contains(engineerId)) {
            return generateRandomNumberWithConstraint(
                numberGenerator,
                consecutiveEngineers
            )
        }

        /**
         * Keep count of occurrences and set priority based on frequency
         */
        if (frequencyMap.containsKey(engineerId)) {
            frequencyMap[engineerId] = frequencyMap[engineerId]!! + 1
            if (frequencyMap[engineerId] == higherFrequency) {
                numberGenerator.addNumber(index, ZERO_PRIORITY)
            }
            if (frequencyMap[engineerId] == lowerFrequency) {
                count++
                if (count > maxFrequencyCount) {
                    numberGenerator.addNumber(index, ZERO_PRIORITY)
                } else {
                    numberGenerator.addNumber(index, LOW_PRIORITY)
                }
            }
        } else {
            frequencyMap.put(engineerId, 1)
            numberGenerator.addNumber(index, NORMAL_PRIORITY)
        }

        return index
    }

}