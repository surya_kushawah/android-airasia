package com.airasia.scheduler.ui.schedule

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.airasia.scheduler.R
import com.airasia.scheduler.core.ViewModelFactory
import com.airasia.scheduler.core.numberOfShift
import com.airasia.scheduler.core.numberOfWeek
import com.airasia.scheduler.data.model.Engineers
import com.airasia.scheduler.ui.schedule.factory.ScheduleFactory
import com.airasia.scheduler.util.Day
import com.airasia.scheduler.util.autoCleared
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.schedule_fragment.*
import javax.inject.Inject

class ScheduleFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private var adapter by autoCleared<ScheduleAdapter>()

    private var listOfEngineers = emptyList<Engineers>()


    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScheduleViewModel::class.java)
    }


    companion object {
        private const val ARG_LIST = "engineers"
        fun newInstance(engineers: ArrayList<Engineers>) =
            ScheduleFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(ARG_LIST, engineers)

                }
            }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            listOfEngineers = it.get(ARG_LIST) as List<Engineers>

        }
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.schedule_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()

        val numberOfDay = Day.values().size * numberOfWeek

        val scheduleFactory = ScheduleFactory(
            listOfEngineers, numberOfDay,
            numberOfShift
        )

        viewModel.getWeeklyShiftSchedule(listOfEngineers, scheduleFactory)

        viewModel.secludeLiveData.observe(this, Observer {
            adapter.submitList(it)
        })

    }

    private fun initRecyclerView() {
        adapter = ScheduleAdapter()
        val layoutManager = LinearLayoutManager(requireContext())
        recyclerViewSchedule.layoutManager = layoutManager
        recyclerViewSchedule.adapter = adapter

    }

}
