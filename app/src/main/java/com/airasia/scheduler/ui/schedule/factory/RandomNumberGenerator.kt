package com.airasia.scheduler.ui.schedule.factory

class RandomNumberGenerator {

    private var priority: MutableMap<Int, Double> = HashMap()
    private var distSum = 0.0

    fun addNumber(value: Int, distribution: Double) {
        if (this.priority[value] != null) {
            distSum -= this.priority[value]!!
        }
        this.priority[value] = distribution
        distSum += distribution
    }

    fun getRandomNumber(): Int {
        val rand = Math.random()
        val ratio = 1.0f / distSum
        var tempDist = 0.0
        for (i in priority.keys) {
            tempDist += priority[i]!!
            if (rand / ratio <= tempDist) {
                return i
            }
        }
        return 0
    }

}