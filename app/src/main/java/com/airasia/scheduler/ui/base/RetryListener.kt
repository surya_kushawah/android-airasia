package com.airasia.scheduler.ui.base

interface RetryListener {
    fun retry()
}