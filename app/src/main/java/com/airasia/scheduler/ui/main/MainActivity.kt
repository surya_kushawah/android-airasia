package com.airasia.scheduler.ui.main

import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProviders
import com.airasia.scheduler.R
import com.airasia.scheduler.core.Navigator
import com.airasia.scheduler.core.ViewModelFactory
import com.airasia.scheduler.ui.base.BaseActivity
import com.airasia.scheduler.ui.schedule.ScheduleFragment
import javax.inject.Inject

class MainActivity : BaseActivity() {
  @Inject
  lateinit var navigator: Navigator
  @Inject
  lateinit var viewModelFactory: ViewModelFactory

  private val mainViewModel: MainViewModel by lazy {
    ViewModelProviders.of(this, viewModelFactory)
        .get(MainViewModel::class.java)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    if (savedInstanceState == null) {
      navigator.navigateEngineers()
    }
  }

  override fun onBackPressed() {
    // this is temporary solution
    val fragment = supportFragmentManager.fragments[supportFragmentManager.backStackEntryCount]
    if (fragment is ScheduleFragment) {
      navigator.navigateEngineers()
    } else {
      super.onBackPressed()
    }
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      android.R.id.home ->
        onBackPressed()
    }
    return true
  }
}
