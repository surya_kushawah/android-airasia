package com.airasia.scheduler.util

enum class Day {
    MON, TUE, WED, THU, FRI, SAT, SUN
}