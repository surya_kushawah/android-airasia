package com.airasia.scheduler.di.component

import android.app.Application
import com.airasia.scheduler.AirAsia
import com.airasia.scheduler.di.module.AppModule
import com.airasia.scheduler.di.module.NetworkModule
import com.airasia.scheduler.di.module.ViewModelModule
import com.airasia.scheduler.ui.main.builder.MainActivityBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        MainActivityBuilder::class
    ]
)
interface AppComponent : AndroidInjector<AirAsia> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(app: AirAsia)
}
