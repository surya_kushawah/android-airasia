package com.airasia.scheduler.di.module

import androidx.lifecycle.ViewModelProvider
import com.airasia.scheduler.core.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
interface ViewModelModule {
    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}