package com.airasia.scheduler.di.module

import com.airasia.scheduler.BuildConfig.BASE_URL
import com.airasia.scheduler.data.remote.EngineersApi
import com.airasia.scheduler.di.NetworkLogger
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

import javax.inject.Singleton

@Module
class NetworkModule {

  @NetworkLogger
  @Singleton
  @Provides
  @IntoSet
  fun provideNetworkLogger(): Interceptor = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY
  }

  @Singleton
  @Provides
  fun provideOkHttpClient(
    @NetworkLogger loggingInterceptors: Set<@JvmSuppressWildcards Interceptor>
  ):
      OkHttpClient =
    OkHttpClient.Builder().apply {
      loggingInterceptors.forEach {
        addNetworkInterceptor(it)
      }
    }.build()

  @Singleton
  @Provides
  fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit =
    Retrofit.Builder().apply {
      client(okHttpClient)
      baseUrl(BASE_URL)
      addConverterFactory(
          MoshiConverterFactory.create(
              Moshi.Builder()
                  .build()
          )
      )
      addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
    }.build()

  @Singleton
  @Provides
  fun provideEngineersApi(retrofit: Retrofit): EngineersApi =
    retrofit.create(EngineersApi::class.java)
}