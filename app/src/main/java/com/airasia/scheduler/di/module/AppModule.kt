package com.airasia.scheduler.di.module

import android.app.Application
import android.content.Context
import com.airasia.scheduler.data.remote.EngineersApi
import com.airasia.scheduler.data.repository.EngineersRepository
import com.airasia.scheduler.data.repository.EngineersRepositoryImpl
import com.airasia.scheduler.util.rx.AppSchedulerProvider
import com.airasia.scheduler.util.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object AppModule {
    @Singleton
    @Provides
    @JvmStatic
    fun provideContext(application: Application): Context = application

    @Singleton
    @Provides
    @JvmStatic
    fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()

    @Singleton
    @Provides
    @JvmStatic
    fun provideEngineersRepository(
        engineersApi: EngineersApi, schedulerProvider: SchedulerProvider
    ): EngineersRepository =
        EngineersRepositoryImpl(engineersApi, schedulerProvider)

}