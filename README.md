# android-airasia
### Project Overview

This project aims is the usage of Android MVVM with [clean architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html). I tried to follow best practices and recommended architecture  by Google.

##### Project Description

First of all, I would like to show how I made the packages structure of the project For achieving The separation of concerns
![alt text](https://developer.android.com/topic/libraries/architecture/images/final-architecture.png)
## Presentation layer
will include normal Activities , Fragments and ViewModels which will only handle rendering views and will follow MVVM pattern.

## Domain layer
With the Use Cases that will include all business logic and interact between Data and Presentation layer by means of interface and interactors. The objective is to make the domain layer independent of anything, so the business logic can be tested without any dependency to external components.

## Data layer
With the Repositories.

## Libraries
- AndroidX Support Library
- AndroidX Architecture Components( ViewModels, LiveData)
- RxJava2
- Dagger2
- Retrofit

## Note
** Few file that are not using this project so please ignore all unused file**

** Dagger is not up mark I'm improving it and try to written feature/module base Dagger**   

